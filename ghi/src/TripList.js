import { useGetTripsQuery, useDeleteTripMutation } from './app/api'
import { NavLink, Link } from 'react-router-dom'

function TripList() {
  const [deleteTrip] = useDeleteTripMutation()
  const { data: tripsData } = useGetTripsQuery()

  if (!tripsData?.trips || tripsData.trips.length === 0) {
    return (
      <div className="no-trip-background font-link">
        <div className="no-trip-text">
          you're lame.
          <br></br>
          you have no trips!
          <br></br>
          <br></br>
        </div>
        <br></br>
        <button className="start-adventure-button">
          <NavLink className="no-trip-link" to="/trips/new">
            start your adventure!
          </NavLink>{' '}
        </button>
      </div>
    )
  }

  return (
    <>
      <div className="triplist-bg-img">
        <div className="font-link">
          <div className="trip-background">
            <div className="create-button">
              <NavLink className="create-trip" to="/trips/new">
                create new trip
              </NavLink>
            </div>
            <div className="trip-title">MY TRIPS</div>
            <div className="trip-table-scroll">
              <table className="trip-table">
                <thead className="trip-header">
                  <tr>
                    <th className="trip-header">NAME</th>
                    <th className="trip-header">RESORT</th>
                    <th className="trip-header">DATE</th>
                    <th className="trip-header">DESCRIPTION</th>
                    <th className="trip-header">CANCEL TRIP</th>
                  </tr>
                </thead>
                <tbody>
                  {tripsData?.trips.map((trip) => {
                    return (
                      <tr key={trip.id}>
                        <td className="trip-row trip-cell">{trip.name}</td>
                        <td className="trip-row trip-cell">
                          <Link
                            className="trip-resort-link"
                            to={`/resorts/${trip.resort_name}`}
                          >
                            {trip.resort_name}
                          </Link>
                        </td>
                        <td className="trip-row trip-cell">
                          {new Date(trip.date).toLocaleDateString('en-US')}
                        </td>
                        <td className="trip-row trip-cell">
                          {trip.description}
                        </td>
                        <td className="trip-row trip-cell">
                          <button
                            className="delete-button"
                            onClick={() => {
                              deleteTrip(trip.id)
                            }}
                          >
                            cancel
                          </button>
                        </td>
                      </tr>
                    )
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default TripList
