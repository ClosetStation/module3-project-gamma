import { useDispatch } from "react-redux";
import { addTrip } from "./app/tripSlice";
import { useAddTripMutation, useGetResortsQuery } from "./app/api";
import { useNavigate } from "react-router-dom";
import { preventDefault } from "./app/utils";
import { useEffect } from "react";

function CreateTripForm() {
  const dispatch = useDispatch();
  const [addTripApi, { data, isSuccess }] = useAddTripMutation();
  const { data: resortsData } = useGetResortsQuery();
  const navigate = useNavigate();
  const onSubmit = preventDefault(async (e) => {
    await addTripApi(e.target);
  });

  useEffect(() => {
    if (data) {
      dispatch(addTrip(data));
    }

    if (isSuccess) {
      setTimeout(() => {
        navigate("/trips");
      }, 0);
    }
  }, [data, isSuccess, dispatch, navigate]);

  return (
    <div className="trip-bg-img">
      <div className="font-link">
        <div className="row create-row">
          <div className="col-sm-6">
            <h1 className="map-text">find a resort near you</h1>
            <iframe
              className="map-create"
              src="https://api.mapbox.com/styles/v1/caitcav/clet0mirb000q01nu6mtzcptk.html?title=false
          &access_token=pk.eyJ1IjoiY2FpdGNhdiIsImEiOiJjbGV0MGpjZG4wNnZxNDBsd2wzb3o5MTRwIn0.T5MeN-H6W8qL24tyF1FWNg&zoomwheel=false
          #4.64/39.75/-119.08"
              title="this is a unique title"
            ></iframe>
          </div>
          <div className="col-md-5">
            <form
              method="post"
              action="/trips"
              onSubmit={onSubmit}
              className="trip-form trip-create-background"
            >
              <h1 className="trip-create-title">create a trip</h1>
              <div className="">
                <label className="trip-form pb-1">name</label>
                <div className="form-group pb-2 trip-form">
                  <input
                    name="name"
                    required
                    className="form-control trip-create-input"
                    type="text"
                    placeholder=""
                  />
                </div>
              </div>
              <div className="trip-form">
                <label className="trip-form pb-1">choose a resort</label>
                <div className="form-group pb-2 trip-form">
                  <select
                    name="resort_name"
                    required
                    className="form-control trip-create-input"
                    type="text"
                    placeholder=""
                  >
                    <option className="trip-form" value="">
                      {" "}
                    </option>
                    {resortsData?.resorts.map((resort, id) => (
                      <option key={id}>{resort.resort_name}</option>
                    ))}
                  </select>
                </div>
              </div>
              <div className="trip-form">
                <label className="trip-form pb-1">dates</label>
                <div className="form-group pb-2 trip-form">
                  <input
                    name="date"
                    className="form-control trip-create-input"
                    type="date"
                    placeholder=""
                  />
                </div>
              </div>
              <div className="trip-form">
                <label className="trip-form pb-1">description</label>
                <div className="form-group pb-2 trip-form">
                  <textarea
                    name="description"
                    className="form-control trip-create-input"
                    placeholder=""
                  />
                </div>
              </div>
              <div className="field is-grouped">
                <div className="">
                  <button className="btn trip-button mx-7">Create</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
export default CreateTripForm;
