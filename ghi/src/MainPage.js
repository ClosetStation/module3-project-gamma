function MainPage() {
  return (
    <div className="main-bg-img">
      <div className="font-link">
        <h3 className="main-title">SNOWBUM</h3>
        <p className="main-text">
          WELCOME! with SNOWBUM you can plan<br></br>
          your next snowboarding trip!<br></br>
          <br></br>
          <br></br>
          Are you a snowbum!?
          <br></br>
          <br></br>
          SnowBum - Someone that eats, breathes, sleeps,<br></br> and lives for
          snowboarding or skiing;<br></br> lives in a shack next to<br></br> the
          nearest resort to get on the<br></br>slopes early and works in a
          <br></br> boardshop to pay for lift tickets.<br></br>
        </p>
      </div>
    </div>
  );
}

export default MainPage;
