import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { clearForm } from './accountSlice'

export const apiSlice = createApi({
  reducerPath: 'api',
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_SNOWBUM_API_HOST,
    prepareHeaders: (headers, { getState }) => {
      const selector = apiSlice.endpoints.getToken.select()
      const { data: tokenData } = selector(getState())
      if (tokenData && tokenData.access_token) {
        headers.set('Authorization', `Bearer ${tokenData.access_token}`)
      }
      return headers
    },
  }),
  tagTypes: ['Account', 'Trips', 'Resorts', 'Weather', 'Token'],
  endpoints: (builder) => ({
    signUp: builder.mutation({
      query: (data) => ({
        url: '/api/accounts',
        method: 'post',
        body: data,
        credentials: 'include',
      }),
      providesTags: ['Account'],
      invalidatesTags: (result) => {
        return (result && ['Token', 'Trips']) || []
      },
      async onQueryStarted(arg, { dispatch, queryFulfilled }) {
        try {
          await queryFulfilled
          dispatch(clearForm())
        } catch (err) {}
      },
    }),
    logIn: builder.mutation({
      query: (info) => {
        let formData = null
        if (info instanceof HTMLElement) {
          formData = new FormData(info)
        } else {
          formData = new FormData()
          formData.append('username', info.username)
          formData.append('password', info.password)
        }
        return {
          url: '/token',
          method: 'post',
          body: formData,
          credentials: 'include',
        }
      },
      providesTags: ['Account'],
      invalidatesTags: (result) => {
        return (result && ['Token', 'Trips']) || []
      },
      async onQueryStarted(arg, { dispatch, queryFulfilled }) {
        try {
          await queryFulfilled
          dispatch(clearForm())
        } catch (err) {}
      },
    }),
    logOut: builder.mutation({
      query: () => ({
        url: '/token',
        method: 'delete',
        credentials: 'include',
      }),
      invalidatesTags: ['Account', 'Token', 'Trips'],
    }),
    getToken: builder.query({
      query: () => ({
        url: '/token',
        credentials: 'include',
      }),
      providesTags: ['Token'],
    }),
    addTrip: builder.mutation({
      query: (form) => {
        const formData = new FormData(form)
        const entries = Array.from(formData.entries())
        return {
          method: 'post',
          url: '/api/trips',
          credentials: 'include',
          body: entries,
        }
      },
      invalidatesTags: [{ type: 'Trips', id: 'LIST' }],
    }),
    getTrips: builder.query({
      query: () => ({ url: `/api/trips`, credentials: 'include' }),
      providesTags: ['Trips'],
    }),
    updateTrip: builder.mutation({
      query: (tripId) => ({
        method: 'post',
        url: `/api/trips/${tripId}`,
      }),
      invalidatesTags: [{ type: 'Trips', id: 'LIST' }],
    }),
    deleteTrip: builder.mutation({
      query: (tripId) => ({
        method: 'delete',
        url: `/api/trips/${tripId}`,
        credentials: 'include',
      }),
      invalidatesTags: [{ type: 'Trips', id: 'LIST' }],
    }),
    getResorts: builder.query({
      query: () => `/api/resorts`,
      providesTags: (data) => {
        const tags = [{ type: 'Resort', id: 'LIST' }]
        if (!data || !data.resorts) return tags

        const { resorts } = data
        if (resorts) {
          tags.concat(...resorts.map(({ id }) => ({ type: 'Resort', id })))
        }
        return tags
      },
    }),
  }),
})
export const {
  useGetTokenQuery,
  useLogInMutation,
  useLogOutMutation,
  useSignUpMutation,
  useAddTripMutation,
  useUpdateTripMutation,
  useGetTripsQuery,
  useDeleteTripMutation,
  useGetResortsQuery,
  useLazyGetWeatherDataQuery,
} = apiSlice
