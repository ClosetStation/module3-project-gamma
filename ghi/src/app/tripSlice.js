import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  trips: [],
  isFetching: false,
  error: null,
}

export const tripSlice = createSlice({
  name: 'trip',
  initialState,
  reducers: {
    addTrip: (state, action) => {
      state.trips.push(action.payload)
    },
    setTrips: (state, action) => {
      state.trips = action.payload
    },
    setIsFetching: (state, action) => {
      state.isFetching = action.payload
    },
    setError: (state, action) => {
      state.error = action.payload
    },
  },
})

export const { addTrip, setTrips, setIsFetching, setError } = tripSlice.actions

export default tripSlice.reducer
