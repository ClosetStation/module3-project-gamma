import { useGetTokenQuery, useLogOutMutation } from "./app/api";
import { useNavigate } from "react-router-dom";
import logo from "./photos/whitelogo.png";
import { useEffect, useState } from "react";

function LoginButtons() {
  const navigate = useNavigate();

  return (
    <div className="font-link">
      <div className="buttons">
        <button
          type="button"
          onClick={() => navigate("/signup")}
          className="btn btn-outline-light logo-box m-3 "
        >
          <i className="bi-snow2" style={{ fontSize: "24px" }}></i>
          <br />
          SIGN UP
        </button>
        <button
          type="button"
          onClick={() => navigate("/login")}
          className="btn btn-outline-light logo-box m-3"
        >
          <i className="bi-sunrise" style={{ fontSize: "24px" }}></i>
          <br />
          LOGIN
        </button>
      </div>
    </div>
  );
}

function LogoutButton() {
  const navigate = useNavigate();
  const [logOut, { data }] = useLogOutMutation();

  useEffect(() => {
    if (data) {
      navigate("/");
    }
  }, [data, navigate]);

  return (
    <div className="font-link">
      <div className="buttons">
        <button
          type="button"
          onClick={logOut}
          className="btn btn-outline-light logo-box m-3"
        >
          <i className="bi-sunset" style={{ fontSize: "24px" }}></i>
          <br />
          LOGOUT
        </button>
      </div>
    </div>
  );
}

function ResortButton() {
  const navigate = useNavigate();

  return (
    <div className="font-link">
      <div className="buttons">
        <button
          type="button"
          onClick={() => navigate("/resorts")}
          className="btn btn-outline-light logo-box m-3"
        >
          <i className="bi-image-alt" style={{ fontSize: "24px" }}></i>
          <br />
          RESORTS
        </button>
      </div>
    </div>
  );
}

function TripButton() {
  const navigate = useNavigate();

  return (
    <div className="font-link">
      <div className="buttons">
        <button
          type="button"
          onClick={() => navigate("/trips")}
          className="btn btn-outline-light logo-box m-3"
        >
          <i className="bi-signpost-split" style={{ fontSize: "24px" }}></i>
          <br />
          TRIPS
        </button>
      </div>
    </div>
  );
}

function LogoButton() {
  const navigate = useNavigate();

  return (
    <div className="font-link">
      <div className="buttons">
        <button
          type="button"
          onClick={() => navigate("/")}
          className="btn btn-outline-light logo-box m-3"
        >
          <img src={logo} height="100" width="100" alt="" />
        </button>
      </div>
    </div>
  );
}

function Nav() {
  const { data: token, isLoading: tokenLoading } = useGetTokenQuery();
  const {
    account: { roles = [] },
  } = token || { account: {} };

  const [joke, setJoke] = useState("");

  const fetchJokeData = async () => {
    const url = `${process.env.REACT_APP_SNOWBUM_API_HOST}/api/jokes`;
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setJoke(data.joke);
    }
  };

  useEffect(() => {
    fetchJokeData();
  }, []);

  return (
    <>
      <ul className="nav nav-tabs" role="tablist">
        <li className="nav-item" role="presentation">
          <LogoButton />
        </li>
        <li className="nav-item" role="presentation">
          <ResortButton />
        </li>
        <li className="nav-item" role="presentation">
          {roles.includes("user") ? <TripButton /> : ""}
        </li>
        <li className="nav-item" role="presentation">
          {tokenLoading ? (
            <LoginButtons show={false} />
          ) : token ? (
            <LogoutButton />
          ) : (
            <LoginButtons show={true} />
          )}
        </li>
        <li className="font-link">
          <div style={{ marginLeft: "400px" }} className="joke-box">
            <p>{joke}</p>
          </div>
        </li>
      </ul>
    </>
  );
}

export default Nav;
