2/13/2023
Set up MongoDB through the docker-compose.yaml file.

2/14/23
Tried to connect to our mongodb and couln't get it figured out. Got our account models done tho.

2/15/23
Finished all routers and queries for accounts. Had a hang up with the account id, but we got it figured out after some digging

2/16/23
Created our Trip model and started working on queries and routers. The date part of the model was problematic.

2/23/23
Got login and logout to work on the front end but the form would not refresh. The biggest problem for me was not using redux before. I think that's where the problem lies

2/24/23
Figured out the login/signup problems. Figured out what forms we wanted and got them created, but no code in there yet. Started to work on importing api data.

2/28/23
Having some authentication problems only when we refrsh a page. Cait pulled together a really cool map to use in our resort page.

3/2/23
Couldn't get all the data we wanted from the api so we decided to cut some stuff out. Shouldn't affect us too much. Most of the data we can't use isn't really neccesary.

3/6/23
Worked on unit tests most of the day. Was having a really hard time with the authenticatiion part. Keep getting 401, 405 and 422 errors.

3/8/23
Spent most of the day getting the readme up to snuff. Made a table for the model data in the readme and it was ROUGH.

3/9/23
Added pictures of our wireframe to our docs file. Luis made a really cool snow falling animation for the resorts page.

3/10/23
Pretty much all done. Been a long road. Now just going through and ironing out any small things before deadline.
