## Journals

Please keep all of your individual journals in this directory.

Each team member is required to keep a development journal, which should be a single markdown file with an entry for each day the project was worked on.

03/08/23

- Project enters its final phase.
  Today, with help of Riley, Cait and Nathan fixed yesterday errors. We almost done with general functionality of app.
  All day we were working with front end and styling. I finished with ResortDetail page and Footer. It looks all right. Louis created animated snowflakes on one of the pages and it looked nice. I definetely will look in animation part of js and react later, it's fun. Tomorrow we'll merge all final work for project assesment into main branch, and continue work on stretch goals in new branch.

03/07/23

- Front-End, Trips list issue with data rendering.

Today we continued work on front-end and tried to fix a bug on trip lists page.
First error is :

**_ return TripList(trips=repo.get_trips(account_id=account_data["id"])) TypeError: 'NoneType' object is not subscriptable _**

and second was:

**_ Access to fetch at 'http://localhost:8000/api/trips' from origin 'http://localhost:3000' has been blocked by CORS policy: No 'Access-Control-Allow-Origin' header is present on the requested resource. If an opaque response serves your needs, set the request's mode to 'no-cors' to fetch the resource with CORS disabled. _**

this error we have almost from beginning. We tried implement session files in separate branch but it didn't work, I'm the one who was responsible for that implementation. I'm pretty sure that I've missed something along the way, even though probably it was not a case for our CORS error. The problem is, if user goes to TripList page at first, it shows user's trips without problems, but as soon user refreshes the page, the data dissapears. In order to see it again, user needs to visit another page and return back on trips list page.

03/06/23

- Front-End

  Today we concentrated on cleaning up some errors on the front-end and creating css. While Luis and Cait was working on the front end, I and Nathan decided to work on unit tests, but we couldn't implement them without errors. In order, not to feel bad about being not productive, I decided to write some css for detail page. Overall, we are close to finish line; as front-end becomes alive, app becomes more pleasingly-looking.

03/03/23

- Front End Whole day.

  today we decided concentrate on the front end, particulary on styling. I tried to create detailed page with card element for the resort using bootstrap. Page doesn't look like as I expected, but tomorrow I will continue to work on it.

03/02/23

- weather api malfunction. fetching detailed resort data on page. fixing some issues that arose
  during process.

  Today we started day from having some issues with functionality on front end. Sign up functionality didn't work.
  some errors we faced today:

  **_ Line 43:6: React Hook useEffect has a missing dependency: 'fetchSnowData'. Either include it or remove the dependency array react-hooks/exhaustive-deps
  2023-03-02 12:37:34 _**

  **_ POST /api/trips/new HTTP/1.1" 405 Method Not Allowed _**

  We couldn't make render a details from our database but eventualy did it. We created a base model for detailed resort, also created endpoint for it and then with fetch method on front end
  maped through and showed everything on page. By 8:30pm we've been able to fix all the problems and we planned spending tomorrow on styling. Overall, great day!

03/01/23

- General functionality of app has been established.

  Today we finished main front-end functionality. List of the resorts from our database has been filtered through. We created a detailed page of resort and feteched 3rd party weather api. Also we fixed the issue with signupModal. The problem was that we misspeled couple fields and it was throwing an error 422.

02/28/23

Missing

02/27/23

Missing

02/24/23

Missing

02/23/23

- Hardest day for me so far.

  We've been able to make authorization work on the back end, but strugled a lot on front end. For me it was poor understanding of redux with react. We could login and logout on front end but the form wouldn't
  refresh, after that. I finished the day very tired and mad at myself.

02/22/23

- Back-end complited. 3rd party API conected to back-end. Front-end authorization start.

  It was productive day. We finished accounts and trips endpoints. Then we concentrated on 3rd party api functionality and with help of Tyler been able to pull the data we neded for mountains endpoint. Additionaly, Nathan found free mountains/resorts data and we've transfered it into our Mongo database. How nice it is!
  At last we went through the front-end authorization and built all neccessary js files for it, but it's not done yet.

02/21/23

- 3rd party api, queries and routers.

  Today we finished some routers and queries endpoints. And then decide to test 3rd party api on back-end.
  We had some errors with it, due to incorrect Queries function code and url. At the end of the day we've been able to resolve it. Tomorrow we need to continue to work on data fetch from api.

02/16/23

- Started working on Trip routers and queries

  Today we created TripQueries and Trip routers. Our Trip modele created some erorrs during testing.
  The problem was in the date instance, the format of which has to be exactly as in documentation.
  We left it for another day.

02/15/23

- Great day!

  Today we continued working on with our account part; finished all routers and
  queries for that. Also we worked on authorization part. At some point we stacked at
  error which was saying that in our create account function:

  ### is not a valid ObjectId, it must be a 12-byte input or a 24-character hex string

  but Cait resolved the issue, and that was great relief. We tested auth. in swager
  and it worked.

02/14/23

- Account models and queries. MongoDB Compass.

  It was pretty intense day for all of us.
  Most of the day we tried to connect MongoDB with our services but couldn't see
  the result.
  Other than that, we created AccountIn and AccountOut.

02/13/23

- Updated Docker-compose file. Added all necessary codes and decided to go with MongoDB.

  We started adding commands in docker-compose file. Used code snippets from Learn. I tried
  to run the docker after that but it didn't worked.
  With help of Tyler been able to fix the issue.
  Mistake: put the external mongo-data in fastapi service section.
  Solution: it has to be before all other services and images.

  Continued updating docker-compose.yaml

02/10/23

- Did practice problems and lectures on data structures and algorithms.

02/09/23

- We finished with wireframing.
- Created API design, except stretch part of the project.
