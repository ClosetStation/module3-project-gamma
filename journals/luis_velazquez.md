02/09/2023

Completed wireframing

02/13/2023

Updated Docker-compose file. Included necessary codes and decided to go with MongoDB. We started adding commands in docker-compose file. Used code snippets from Learn.

02/14/2023

Currently struggling with MongoDB. Helped tried multiple codes to figure our MongoDB on queries and routers to get something to pop out.
Problem: (Not sure!) Database configuration wasn't working properly.

02/15/2023

Helped fixed previous error involving AccountIn/AccountOut functionality. Added authenticators in the router section
and completed authorization! EVERYTHING WORKS.

02/21/2023

Able to pull information from third-party API with a function to get_mountains_by_name by editing/adding into - main.py, routers, queries, and keys.py.

main.py: was to include a mountain router connection
routers: added a file "mountains.py" that defines an API endpoint for get_mountains_by_name accessing the '/api/mountains/' route with a "name" parameter for the function to execute. We added two parameters(name-name of mountains to search for, repo-istance of the 'MountainQueries' class through Depends() from FastAPI)
queries: added a file "mountains.py" with a class called "MountainQueries" with get_mountains_by_name function that retrieves information about mountains with the given name through request to third-part API
keys: third-party API key to be used in queries as headers



02/09/2023:

We created models for the accounts and the create account function and router on the backend.

02/14/2023:

We struggled to connect MongoDB with our code and faced some difficulties getting it to work. We hope for a better day tomorrow.

02/15/2023:

We successfully connected our database to our code and started creating accounts. Tomorrow we plan to work on authentication.

02/16/2023:

We began working on authentication and almost completed it. It's going well so far and hasn't caused any major issues.

02/21/2023:

We spent the day working on the other endpoints and faced some errors while using authentication. Additionally, we set up our third-party API and managed to retrieve mountain information.

02/22/2023:

Today was a huge day as we completed the backend of our web application! It feels amazing to have worked as a team and gotten this far. We all had a hand in each piece of the code and are excited to begin the frontend tomorrow.

02/23/2023:

We started working on the frontend today. Our plan is to create all the code first and style it later. We began working on authentication for the login and sign up pages.

02/24/2023:

We successfully created a sign-up and login page! We created all the form pages and began working on the mountain detail pages so we could connect our third-party API. We encountered some bugs, but were able to work as a team to resolve them.

02/27/2023:

Honestly dont remember. This monday hit me hard.

02/28/2023:

We continued to work on the trips list and create trip form. We experienced some authentication errors when refreshing the page, but we are working to resolve them. We are adding more information to the mountain detail page, including an interactive map.

03/01/2023:

We spent most of the day working on the mountain list and detail pages to ensure we could retrieve all the information we wanted for the mountain page. We discovered a weird crossover with one of our APIs, so we won't be using both. They use the same API key and were causing problems.

03/02/2023:

We encountered errors with our third-party API today, so we commented out the code for now. We are waiting to see if the API will function properly again soon. In the meantime, we created a collection in our database with a lot of mountain information that we can retrieve.

03/03/2023:

Today was CSS day! We began playing with styling since our website is almost fully functional. We are excited to make it look pretty. I'm in love with all the functionality.

03/06/2023:

Today was a SCCS with CSS and Bootstrap. Two worked on styling while the other two worked on unit tests. Tomorrow will be a big unit test day now that the CSS is almost complete. EVEYTHING IS COMING TOGETHER!!!!

03/07/2023:

Some minor errors today, such as: date, refreshing, table info not updating, styling issues. Working on each one as a team!!!!

03/08/2023:

Fixing issues with a little more CSS

03/09/2023:

We got snowflake, everyone! We got snowflakes, but doesn't for some reason the links placed on the body of the page wouldn't work. So, we decided to move the links in the footer to avoid the this problem.

03/10/2023:

Last touches before turning it in!
