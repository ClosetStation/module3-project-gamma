February 8, 2023

Completed the wireframing

February 13, 2023

Started work on the back end, started by creating models for the accounts and the create account
function and router.


February 14, 2023

Tried connecting MongoDB with our code and had a lot of trouble getting it to work.
We are hoping that tomorrow is a better day!

February 15, 2023

We got our database to connect to our code and were able to start creating accounts
so tomorrow we can start working on authentication.

February 16, 2023

Started on authentication and almost got finished with it - very excited that it is
all going well and hasn't completely broken our code!

February 21, 2023

Spent the day working on the other endpoints and ran into a few errors while using
authentication. We also set up our third party api and managed to get data for the
mountains information!

February 22, 2023

HUGE DAY!! We finished the back end of the web application!!!! It feels AMAZING to get here and have worked as team to make it happen. We have all had our hands on all pieces of the code and are looking forward to starting the front end tomorrow!

February 23, 2023

Started on the front end!! We have decided to create all the code first and start styling later. Today we started on the authentication of the login and sign up pages!

February 24, 2023

We have managed to create a sign up and login page that works successfully! We have created all the form pages and have begun to work on the mountain detail pages so we can connect our third party api. All of this has been causing bugs, but we have been able to work as a team to get that together and figure it out.

February 27, 2023

Was not in class.

February 28, 2023

Continued to work on the trips list and the create trip form. We are running into some weird authentication errors when we refresh the page so we are trying to make it work. But we are also finally getting full functionality and adding more info to our mountain detail page (plus an interactive map!)

March 1, 2023

Today was spent mostly on the mountain list and detail pages to make sure we could get all the info we wanted on the mountain page. We realized that one of our apis has a weird crossover so we aren't going to be using both (they use the same api key and are causing problems)

March 2, 2023

Had more weird errors with our third party api today so we decided to comment out the code (for now) so we can make sure everything else is working - the api seems to be down today, but we are hoping it'll be back soon! We also created a collection in our database with a ton of mountain info that we can pull from while waiting to see if the third party api will play nice.

March 3, 2023

CSS Day!!! Now that our website is almost fully functional we have started to play with styling and we are really excited to get it all looking pretty! We are really pleased with all the functionality.

March 6, 2023

HUGE HUGE CSS and bootstrap day. Two of us worked on the css and styling while two of us worked on the unit tests. Tomorrow is going to be a big unit test day now that the css is getting toward the end. I'm excited to see the final product and I'm really impressed with the spaghetti squad on all we have accomplished!

March 7, 2023

Today we spent the day trying to get our trips to work. At the beginning of the day we worked on our unit tests and got all of them to run the way we wanted! Everything passed and that felt awesome. Then we spent the afternoon fixing an error with our trip list page - we were getting CORS errors. We did manage to get it fixed and now the site is running without errors.


March 8, 2023

More CSS and little details to make it all look good!

March 9, 2023

Did a big push on the resort detail page to get it looking nice and running with the snow falling (thanks Luis!!!). I am so excited that it is all coming together and finally super funcational.

March 10, 2023

IT IS DONE and I am so, so excited about it. I feel so good about this project and how my team worked. I cannot wait to show it off to everyone! Today we spent the day fixing a cache issue, but we got it to run smoothly.

We have worked amazing as a team this entire project. We have had a ton of fun and have spent a lot of the time enjoying the process and really spending the time learning together. I cannot wait to make my next site! It was really hard, but I'm proud of it.

March 15, 2023

DEPLOYMENT 
