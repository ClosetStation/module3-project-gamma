from models import TripIn, TripOut, TripList
from queries.client import Queries
from pymongo import ReturnDocument
from bson.objectid import ObjectId


class TripQueries(Queries):
    DB_NAME = "library"
    COLLECTION = "trips"

    def get_trips(self, account_id: str) -> TripList:
        trips = self.collection.find({"account_id": account_id})
        all_trips = []
        for trip in trips:
            trip["id"] = str(trip["_id"])
            all_trips.append(TripOut(**trip))
        return all_trips

    def get_trip(self, id: int) -> TripOut:
        result = self.collection.find_one({"_id": id})
        if result:
            result["id"] = str(result["_id"])
        return TripOut(**result)

    def create_trip(self, trip: TripIn, account_id: str) -> TripOut:
        trip_to_add = trip.dict()
        trip_to_add["account_id"] = account_id
        result = self.collection.insert_one(trip_to_add)
        if result.inserted_id:
            result = self.get_trip(result.inserted_id)
            return result

    def update_trip(self, trip: TripIn, id: str):
        trip_to_update = self.collection.find_one_and_update(
            {"_id": ObjectId(id)},
            {"$set": trip.dict()},
            return_document=ReturnDocument.AFTER,
        )
        if trip_to_update:
            return TripOut(**trip_to_update, id=id)

    def delete_trip(self, id):
        trip_to_delete = self.collection.delete_one(
            filter={"_id": ObjectId(id)}
        )
        if trip_to_delete:
            return {"Success"}
