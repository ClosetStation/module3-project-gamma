from queries.client import Queries
from models import AccountsIn, AccountsOut
from bson.objectid import ObjectId
from pymongo.errors import DuplicateKeyError
from pymongo import ReturnDocument


class DuplicateAccountError(ValueError):
    pass


class AccountQueries(Queries):
    DB_NAME = "library"
    COLLECTION = "accounts"

    def get(self, username: str) -> AccountsOut:
        info = self.collection.find_one({"username": username})
        if not info:
            return None
        info["id"] = str(info["_id"])
        return AccountsOut(**info)

    def create(
        self, account: AccountsIn, hashed_password: str, roles=["user"]
    ) -> AccountsOut:
        account = account.dict()
        account["password"] = hashed_password
        account["roles"] = roles
        try:
            self.collection.insert_one(account)
        except DuplicateKeyError:
            raise DuplicateAccountError()
        account["id"] = str(account["_id"])
        return AccountsOut(**account)

    def update_account(self, account: AccountsIn):
        account_to_update = self.collection.find_one_and_update(
            {"_id": ObjectId(id)},
            {"$set": account.dict()},
            return_document=ReturnDocument.AFTER,
        )
        if account_to_update:
            return AccountsOut(**account_to_update, id=id)
