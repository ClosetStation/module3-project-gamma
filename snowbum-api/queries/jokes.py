import requests
from models import Joke


class JokeQueries:
    def get_joke(self) -> Joke:
        headers = {"Accept": "application/json"}
        response = requests.get("https://icanhazdadjoke.com/", headers=headers)
        return Joke(**response.json())
