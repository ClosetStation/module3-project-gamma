import requests
import os
from queries.client import Queries
from models import ResortList, Resort, ResortDetail
RESORT_API_KEY = os.environ["RESORT_API_KEY"]


class ForecastQueries:
    def get_resort_forecast(self, resort_name: str):
        result = requests.get(
            "https://ski-resort-forecast.p.rapidapi.com/"
            + resort_name
            + "/forecast",
            headers={
                "X-RapidAPI-Key": RESORT_API_KEY,
                "X-RapidAPI-Host": "ski-resort-forecast.p.rapidapi.com",
            },
        )
        data = result.json()
        return data["imperial"]["topLift"]


class ResortQueries(Queries):
    DB_NAME = "library"
    COLLECTION = "mountains"

    def get_resorts(self) -> ResortList:
        mountains = self.collection.find()
        all_mountains = []
        for mountain in mountains:
            all_mountains.append(Resort(**mountain))
        return all_mountains

    def get_one_resort(self, resort_name: str):
        result = self.collection.find_one({"resort_name": resort_name})
        if result:
            result["resort_name"] = str(result["resort_name"])
        return ResortDetail(**result)
