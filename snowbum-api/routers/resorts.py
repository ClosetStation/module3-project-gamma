from queries.resorts import ResortQueries, ForecastQueries
from fastapi import APIRouter, Depends
from models import ResortList, ResortDetail

router = APIRouter()


@router.get("/api/weather/{resort_name}")
def get_resort_forecast(
    resort_name: str,
    repo: ForecastQueries = Depends(),
):
    return repo.get_resort_forecast(resort_name=resort_name)


@router.get("/api/resorts")
async def get_resorts(
    repo: ResortQueries = Depends(),
):
    return ResortList(resorts=repo.get_resorts())


@router.get("/api/resorts/{resort_name}", response_model=ResortDetail)
async def get_one_resort(
    resort_name: str,
    repo: ResortQueries = Depends(),
):
    return repo.get_one_resort(resort_name)
