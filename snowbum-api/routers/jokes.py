from fastapi import Depends
from queries.jokes import JokeQueries
from fastapi import APIRouter

router = APIRouter()


@router.get("/api/jokes")
def get_joke(repo: JokeQueries = Depends()):
    return repo.get_joke()
