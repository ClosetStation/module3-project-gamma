from queries.trips import TripQueries
from models import TripList, TripOut, TripIn
from fastapi import APIRouter, Depends, Response, Request
from auth import authenticator

router = APIRouter()


@router.get("/api/trips")
async def get_trips(
    repo: TripQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return TripList(trips=repo.get_trips(account_id=account_data["id"]))


@router.get("/api/trips/{id}", response_model=TripOut)
async def get_trip(
    id: str,
    response: Response,
    repo: TripList = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    trip = repo.get_trip(id, account_id=account_data["id"])
    if trip is None:
        response.status_code = 404
    return trip


@router.post("/api/trips", response_model=TripOut)
async def create_trip(
    info: TripIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: TripQueries = Depends(),
):
    return repo.create_trip(trip=info, account_id=account_data["id"])


@router.put("/api/trips/{id}", response_model=TripOut)
async def update_trip(
    id: str,
    request: Request,
    trip: TripIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: TripQueries = Depends(),
):
    if account_data and authenticator.cookie_name in request.cookies:
        return repo.update_trip(trip=trip, id=id)
    else:
        return "Not working"


@router.delete("/api/trips/{id}")
async def delete_trip(
    request: Request,
    id: str,
    repo: TripQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    if account_data and authenticator.cookie_name in request.cookies:
        repo.delete_trip(id=id)
        return True
    else:
        return "Not working"
