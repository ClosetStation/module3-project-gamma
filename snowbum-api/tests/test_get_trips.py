from fastapi.testclient import TestClient
from main import app
from queries.trips import TripQueries
from routers.auth import authenticator


client = TestClient(app)


def fake_get_account_data():
    return {
        "id": "42",
        "username": "nag",
        "roles": "user",
        "email": "str",
    }


class FakeTripQueries:
    def get_trips(self, account_id: str):
        return []


def test_get_trips():
    app.dependency_overrides[TripQueries] = FakeTripQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_account_data

    res = client.get("/api/trips")
    data = res.json()

    assert res.status_code == 200
    assert data["trips"] == []
