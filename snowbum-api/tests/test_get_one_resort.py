from fastapi.testclient import TestClient
from main import app
from queries.resorts import ResortQueries

client = TestClient(app)


class FakeResortQueries:
    def get_one_resort(self, resort_name: str):
        return {
            "resort_name": resort_name,
            "state": "Colorado",
        }


def test_get_one_resort():
    app.dependency_overrides[ResortQueries] = FakeResortQueries

    res = client.get("/api/resorts/aspen")
    data = res.json()

    assert data["resort_name"] == "aspen"
    assert res.status_code == 200
