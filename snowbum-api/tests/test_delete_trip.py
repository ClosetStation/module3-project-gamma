from fastapi.testclient import TestClient
from queries.trips import TripQueries
from main import app
from routers.auth import authenticator

client = TestClient(app)


def fake_get_current_account_data():
    return {
        "id": "42",
        "username": "fakeuser",
        "roles": "user",
        "email": "str",
    }


class FakeTripsQueries:
    def delete_trip(self, id: str):
        return {"id": id}


def test_delete_trip():
    app.dependency_overrides[TripQueries] = FakeTripsQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    res = client.delete("/api/trips/42")

    assert res.status_code == 200
