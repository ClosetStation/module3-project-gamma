from fastapi.testclient import TestClient
from main import app
from queries.resorts import ResortQueries

client = TestClient(app)


class FakeResortQueries:
    def get_resorts(self):
        return []


def test_get_resorts():
    app.dependency_overrides[ResortQueries] = FakeResortQueries

    res = client.get("/api/resorts")
    data = res.json()

    assert data["resorts"] == []
    assert res.status_code == 200
