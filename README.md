Snowbum

Nathan Grimes
Soslan Tmenov
Luis Velazquez
Cait Wondrasch


Snowbum – bringing you all of your favorite mountains and ski resorts all in one place
Snowbum - Choose your own adventure

Design

API design
Data model
GHI
Integrations

Please check our docs directory for all documentation about the Snowbum


Intended market
We are targeting snowboarders from all skill levels and backgrounds who are looking for a place to come together and plan sbowboarding trips of a lifetime.


Functionality

Visitors to the site can view more than 200 mountains from all across the U.S. and view upcoming weather conditions for each mountain

Users of the site can view each mountain and see a map of all the snowboard and ski routes for each specifc mountain

Added a table for each mountain with various data about the ski resort such as summit, lifts, route acres, etc.

Included a map of to see what mountains are near your location


Accounts

Users can create a trip to the mountain of thier choice with set dates and a short description of your trip

Main page featues a brief description about the service



Project Initialization

To fully enjoy this application on your local machine, please make sure to follow these steps:

Clone the repository down to your local machine

CD into the new project directory

Open the project into VS Code or the code program of your choosing

Go to https://rapidapi.com/joeykyber/api/ski-resort-forecast/ and sign up to receive an API Key to the free api service

Once you have received the key create a keys.py file in the snowbum_api directory and put the api key in there with the
variable name "RESORT_API_KEY" ex: RESORT_API_KEY = "API_KEY"

Run docker volume create mongo-db

Run docker compose build

Run docker compose up

Run docker exec -it snow_bumapi-1 bash

Exit the container's CLI, and enjoy Snowbum to its fullest!
